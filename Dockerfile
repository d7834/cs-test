FROM node:20-alpine3.16

WORKDIR $HOME/app/

COPY scripts/* .
COPY scan.sh .
RUN chmod +x scan.sh

RUN npm install typescript -g
RUN npm install
RUN tsc

ENV SCA_VERSION=0.0.55
ENV SAST_VERSION=0.0.43

# Install Lacework CLI
RUN apk update && apk add curl && apk add bash && apk add git && apk add openssh
RUN curl https://raw.githubusercontent.com/lacework/go-sdk/main/cli/install.sh | bash

ENTRYPOINT [ "./scan.sh" ]
