import { callLaceworkCli, getRequiredEnvVariable, debug, callCommand } from './util';
import { existsSync } from 'fs'
import { compareResults, printResults } from './tool';
import { Gitlab } from '@gitbeaker/rest';

const scaReport = 'lw_sca.sarif'
const sastReport = 'lw_sast.sarif'

const gitlabApi = new Gitlab({
  token: getRequiredEnvVariable("GITLAB_TOKEN"),
})

async function runAnalysis(ciTrigger: string) {
  const tools = getRequiredEnvVariable('LW_TOOLS').toLowerCase().split(',')

  let reportFormat = 'sarif';
  if (process.env.LW_REPORT_FORMAT === 'gitlab-json') {
    reportFormat = 'gitlab-json'
  }

  if (tools.includes('sca')) {
    const indirectDeps = getRequiredEnvVariable('LW_SCA_EVAL_INDIRECT_DEPENDENCIES');
    if (ciTrigger === 'merge_request_event') {
      const ciSourceBranch = getRequiredEnvVariable('CI_MERGE_REQUEST_SOURCE_BRANCH_NAME');
      const ciTargetBranch = getRequiredEnvVariable('CI_MERGE_REQUEST_TARGET_BRANCH_NAME');
      console.info(`CI Source Branch: ${ciSourceBranch}`)
      console.info(`CI Target Branch: ${ciTargetBranch}`)

      //source branch
      var args = [
        'sca',
        'git',
        '.',
        '--save-results',
        '-o',
        `${ciSourceBranch}-${scaReport}`,
        '--formats',
        reportFormat,
        '--deployment',
        'ci',
        '--secret',
      ]
      if (indirectDeps.toLowerCase() === 'false') {
        args.push('--eval-direct-only')
      }
      if (debug()) {
        args.push('--debug')
      }
      // console.info()
      await callLaceworkCli(...args)

      await callCommand('git', 'fetch', 'origin')
      // Gitlab makes a commit which represents the HEAD of the target branch at the time that the MR was created
      // this is the base that we should analyse as the target when comparing source vs target
      const mergeRequestDiffSha = getRequiredEnvVariable("CI_MERGE_REQUEST_DIFF_BASE_SHA")
      console.info('Analysing merge request Diff SHA commit', mergeRequestDiffSha)
      // now we've analysed the source branch, let's checkout the target
      await callCommand('git', 'checkout', mergeRequestDiffSha)

      //target branch
      var args = [
        'sca',
        'git',
        '.',
        '--save-results',
        '-o',
        `${ciTargetBranch}-${scaReport}`,
        '--formats',
        reportFormat,
        '--deployment',
        'ci',
        '--secret',
        // '--no-scr'
      ]
      if (indirectDeps.toLowerCase() === 'false') {
        args.push('--eval-direct-only')
      }
      if (debug()) {
        args.push('--debug')
      }
      await callLaceworkCli(...args)

      const issuesByTool: { [tool: string]: string } = {}
      if (existsSync(`${ciSourceBranch}-${scaReport}`) && existsSync(`${ciTargetBranch}-${scaReport}`)) {
        issuesByTool['sca'] = await compareResults(
          'sca',
          `${ciTargetBranch}-${scaReport}`,
          `${ciSourceBranch}-${scaReport}`
        )
        console.log(issuesByTool['sca'])
        addMergeRequestComment(issuesByTool['sca'])
      }
      // push to branch
    } else {
      var args = [
        'sca',
        'git',
        '.',
        '--save-results',
        '-o',
        scaReport,
        '--formats',
        reportFormat,
        '--deployment',
        'ci',
        '--secret',
      ]
      if (indirectDeps.toLowerCase() === 'false') {
        args.push('--eval-direct-only')
      }
      if (debug()) {
        args.push('--debug')
      }
      await callLaceworkCli(...args)
      await printResults('sca', scaReport, reportFormat)
    }
  }
  if (tools.includes('sast')) {
    console.error('Sorry we do not support SAST scanning yet')
    process.exit(0)
    // if we're running in an MR
    // if (ciTrigger === 'merge_request_event') {
    //   const ciSourceBranch = getRequiredEnvVariable('CI_MERGE_REQUEST_SOURCE_BRANCH_NAME');
    //   const ciTargetBranch = getRequiredEnvVariable('CI_MERGE_REQUEST_TARGET_BRANCH_NAME');
    //   console.info(`CI Source Branch: ${ciSourceBranch}`)
    //   console.info(`CI Target Branch: ${ciTargetBranch}`)

    //   var args = [
    //     'sast',
    //     'scan',
    //     '--save-results',
    //     '-o',
    //     `${ciTargetBranch}-${scaReport}`,
    //     '--formats',
    //     'sarif',
    //     '--classes',
    //     getRequiredEnvVariable('LW_SAST_CLASSES'),
    //     '--sources',
    //     getRequiredEnvVariable('LW_SAST_SOURCES'),
    //     '-o',
    //     `${ciSourceBranch}-${sastReport}`,
    //     '--deployment',
    //     'ci',
    //   ]
    //   if (debug()) {
    //     args.push('--debug')
    //   }
    //   await callLaceworkCli(...args)
    //   // SAST doesn't support gitlab-json output yet
    //   await printResults('sast', sastReport, 'sarif')

    //   await callCommand('git', 'fetch', 'origin')
    //   // Gitlab makes a commit which represents the HEAD of the target branch at the time that the MR was created
    //   // this is the base that we should analyse as the target when comparing source vs target
    //   const mergeRequestDiffSha = getRequiredEnvVariable("CI_MERGE_REQUEST_DIFF_BASE_SHA")
    //   console.info('Analysing merge request Diff SHA commit', mergeRequestDiffSha)
    //   // now we've analysed the source branch, let's checkout the target
    //   await callCommand('git', 'checkout', mergeRequestDiffSha)

    //   //target branch
    //   var args = [
    //     'sast',
    //     'scan',
    //     '--save-results',
    //     '--classes',
    //     getRequiredEnvVariable('LW_SAST_CLASSES'),
    //     '--sources',
    //     getRequiredEnvVariable('LW_SAST_SOURCES'),
    //     '-o',
    //     `${ciTargetBranch}-${sastReport}`,
    //     '--formats',
    //     'sarif',
    //     '--deployment',
    //     'ci'
    //   ]
    //   if (debug()) {
    //     args.push('--debug')
    //   }
    //   await callLaceworkCli(...args)

    //   const issuesByTool: { [tool: string]: string } = {}
    //   if (existsSync(`${ciSourceBranch}-${sastReport}`) && existsSync(`${ciTargetBranch}-${sastReport}`)) {
    //     issuesByTool['sast'] = await compareResults(
    //       'sast',
    //       `${ciTargetBranch}-${sastReport}`,
    //       `${ciSourceBranch}-${sastReport}`
    //     )
    //     console.log(issuesByTool['sast'])
    //     addMergeRequestComment(issuesByTool['sast'])
    //   }
    // } else {
    //   var args = [
    //     'sast',
    //     'scan',
    //     '--save-results',
    //     '--classes',
    //     getRequiredEnvVariable('LW_SAST_CLASSES'),
    //     '--sources',
    //     getRequiredEnvVariable('LW_SAST_SOURCES'),
    //     '-o',
    //     sastReport,
    //     '--deployment',
    //     'ci',
    //   ]
    //   if (debug()) {
    //     args.push('--debug')
    //   }
    //   await callLaceworkCli(...args)
    //   // SAST doesn't support gitlab-json output yet
    //   await printResults('sast', sastReport, 'sarif')
    // }
  }
}

async function addMergeRequestComment(comment: string) {
  if (comment === '') {
    return;
  }
  const ciProjectId = getRequiredEnvVariable('CI_PROJECT_ID')
  const mergeRequestIid: number = parseInt(getRequiredEnvVariable('CI_MERGE_REQUEST_IID'))
  gitlabApi.MergeRequestNotes.create(ciProjectId, mergeRequestIid, comment)
}

const ciTrigger = process.env.CI_PIPELINE_SOURCE;
if (ciTrigger === 'merge_request_event') {
  const ciSourceBranch = getRequiredEnvVariable('CI_MERGE_REQUEST_SOURCE_BRANCH_NAME');
  const ciTargetBranch = getRequiredEnvVariable('CI_MERGE_REQUEST_TARGET_BRANCH_NAME');
  console.log(`Pipeline triggered by merge request from branch ${ciSourceBranch} to ${ciTargetBranch}`);
  // TODO: run an analysis on merge request event
  runAnalysis(ciTrigger)
} else if (ciTrigger === 'push') {
  const ciBranch = getRequiredEnvVariable("CI_COMMIT_BRANCH");
  console.log(`Pipeline triggered by push to branch ${ciBranch}`)
  runAnalysis(ciTrigger)
}