import { spawnSync, spawn } from 'child_process'

function getBooleanInput(name: string): boolean {
    const debug = process.env[name];
    if(debug) {
        return debug.toLowerCase() === 'true'
    }
    return false;
}

export function debug(): boolean {
    return getBooleanInput('LW_DEBUG')
}

// export async function callCommand(command: string, ...args: string[]) {
//     const child = spawnSync(command, args)
//     if (child.stderr !== null && child.stderr.toString() !== '') {
//       console.info(`stderr from command:\n${child.stderr.toString()}`)
//     }
//     if (child.status) {
//       console.error(`Failed with status ${child.status}`)
//       process.exit(0) // TODO: Exit with 1 once we want failures to be fatal
//     }
//     return child.stdout.toString().trim()
// }

export async function callCommand(command: string, ...args: string[]) {
  console.info('Invoking ' + command + ' ' + args.join(' '))
  const child = spawn(command, args, { stdio: 'inherit' })
  const exitCode = await new Promise((resolve, _) => {
    child.on('close', resolve)
  })
  if (exitCode !== 0) {
    console.error(`Command failed with status ${exitCode}`)
    throw new Error(`Command failed with status ${exitCode}`)
  }
}

export function getRequiredEnvVariable(name: string) {
    const value = process.env[name]
    if (!value) {
      console.error(`Missing required environment variable ${name}`)
      process.exit(0) // TODO: Exit with 1 once we want failures to be fatal
    }
    return value
}

export async function callLaceworkCli(...args: string[]) {
    const accountName = getRequiredEnvVariable('LW_ACCOUNT')
    const apiKey = getRequiredEnvVariable('LW_API_KEY')
    const apiSecret = getRequiredEnvVariable('LW_API_SECRET')
    const expandedArgs = [
      '--noninteractive',
      '--account',
      accountName,
      '--api_key',
      apiKey,
      '--api_secret',
      apiSecret,
      ...args,
    ]
    console.info('Calling lacework ' + redactApiCredentials(expandedArgs.join(' ')))
    return await callCommand('lacework', ...expandedArgs)
}

function redactApiCredentials(inputString: string): string {
  const redactedString = inputString.replace(/(?<=\s--api_key\s)(\S+)/g, "REDACTED")
                                    .replace(/(?<=\s--api_secret\s)(\S+)/g, "REDACTED");
  return redactedString;
}
